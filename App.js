import React, { Component } from 'react';
import { View, Text } from 'react-native';
import BasicFlatList from './components/BasicFlatList';
import HorizontalFlatList from './components/HorizontalFlatList';
import VerticalScrollView from './components/VerticalScrollView';

export default class App extends Component {
  render() {
    return (
      <View>
        {/* <BasicFlatList /> */}
        {/* <HorizontalFlatList /> */}
        <VerticalScrollView/>
      </View>
    )
  }
}