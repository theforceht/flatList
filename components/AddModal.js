import React, { Component } from 'react';
import {
  View, Text, Dimentions, FlatList, Image, TouchableHighlight, TextInput,
  Platform, Alert, StyleSheet, Dimensions
} from 'react-native';

import Modal from 'react-native-modalbox';
import Button from 'react-native-button';
import flatListData from '../data/flatListData';

var screen = Dimensions.get('window');

export default class AddModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newChampionName: '',
      newChampionDescription: '',
    }
  }

  generateKey = (numberOfCharacters) => {
    return (require('random-string')({ length: numberOfCharacters }));
  }
  _onPressSave = () => {
    if (this.state.newChampionName.length == 0 || this.state.newChampionDescription.length == 0) {
      alert("You must enter champion's name and description");
      return;
    }
    const newKey = this.generateKey(17);
    const newChampion = {
      key: newKey,
      name: this.state.newChampionName,
      imageUrl: "https://image.thanhnien.vn/660/uploaded/vietthong/2019_01_11/thumb_hpkn.jpg",
      championDescription: this.state.newChampionDescription
    }
    flatListData.push(newChampion);
    this.refs.myModal.close();
    this.props.parentFlatList.refreshFlatList(newKey);
    alert("Save");
  }

  showAddModal = () => {
    this.refs.myModal.open();
  }

  render() {
    return (
      <Modal
        ref={"myModal"}
        style={{
          justifyContent: 'center',
          borderRadius: Platform.OS === "ios" ? 30 : 0,
          shadowRadius: 10,
          width: screen.width - 80,
          height: 240
        }}
        position='center'
        backdrop={true}
        onClosed={() => {
        }}
      >
        <Text
          style={{
            fontWeight: 'bold',
            textAlign: 'center',
            fontSize: 16,
            marginBottom: 10
          }}>
          New champion's infomation
        </Text>
        <TextInput
          style={styles.textInput}
          placeholder="Enter new champion's name"
          value={this.state.newChampionName}
          onChangeText={(text) => { this.setState({ newChampionName: text }) }} />
        <TextInput
          style={styles.textInput}
          placeholder="Enter desciption"
          value={this.state.newChampionDescription}
          onChangeText={(text) => { this.setState({ newChampionDescription: text }) }}
        />
        <Button
          style={{
            color: 'white',
            fontSize: 16,
            padding: 10,
            marginTop: 20,
            marginLeft: 70,
            marginRight: 70,
            backgroundColor: 'mediumseagreen',
            borderRadius: 5
          }}
          onPress={this._onPressSave}>
          Save
        </Button>

      </Modal>
    )
  }
}
const styles = StyleSheet.create({
  textInput: {
    borderBottomWidth: 1,
    padding: 5,
    marginLeft: 20,
    marginRight: 20,
    marginTop: 10,

  }
})