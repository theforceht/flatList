import React, { Component } from 'react';
import {
  View, Text, Dimentions, FlatList, Image, TouchableHighlight, TextInput,
  Platform, Alert, StyleSheet, Dimensions
} from 'react-native';

import Modal from 'react-native-modalbox';
import Button from 'react-native-button';
import flatListData from '../data/flatListData';

var screen = Dimensions.get('window');

export default class EditModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      championName: '',
      championDescription: '',
    }
  }

  generateKey = (numberOfCharacters) => {
    return (require('random-string')({ length: numberOfCharacters }));
  }
  _onPressSave = () => {
    if (this.state.championName.length == 0 || this.state.championDescription.length == 0) {
      alert("You must enter champion's name and description");
      return;
    }
    //updating champion
    var foundIndex = flatListData.findIndex(item => this.state.key == item.key);
    if (foundIndex < 0) return;
    flatListData[foundIndex].name = this.state.championName;
    flatListData[foundIndex].championDescription = this.state.championDescription;
    this.state.flatListItem.refreshFlatListItem();
    this.refs.myModal.close();
  }

  showEditModal = (editingChampion, flatListItem) => {
    this.setState({
      key: editingChampion.key,
      championName: editingChampion.name,
      championDescription: editingChampion.championDescription,
      flatListItem: flatListItem,
    })
    this.refs.myModal.open();
  }

  render() {
    return (
      <Modal
        ref={"myModal"}
        style={{
          justifyContent: 'center',
          borderRadius: Platform.OS === "ios" ? 30 : 0,
          shadowRadius: 10,
          width: screen.width - 80,
          height: 240
        }}
        position='center'
        backdrop={true}
        onClosed={() => {
        }}
      >
        <Text
          style={{
            fontWeight: 'bold',
            textAlign: 'center',
            fontSize: 16,
            marginBottom: 10
          }}>
          Champion's infomation
        </Text>
        <TextInput
          style={styles.textInput}
          placeholder="Enter Champion's name"
          value={this.state.championName}
          onChangeText={(text) => { this.setState({ championName: text }) }} />
        <TextInput
          style={styles.textInput}
          placeholder="Enter Desciption"
          value={this.state.championDescription}
          onChangeText={(text) => { this.setState({ championDescription: text }) }}
        />
        <Button
          style={{
            color: 'white',
            fontSize: 16,
            padding: 10,
            marginTop: 20,
            marginLeft: 70,
            marginRight: 70,
            backgroundColor: 'mediumseagreen',
            borderRadius: 5
          }}
          onPress={this._onPressSave}>
          Save
        </Button>

      </Modal>
    )
  }
}
const styles = StyleSheet.create({
  textInput: {
    borderBottomWidth: 1,
    padding: 5,
    marginLeft: 20,
    marginRight: 20,
    marginTop: 10,

  }
})