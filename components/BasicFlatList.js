import React, { Component } from 'react';
import { View, Text, Image, FlatList, StyleSheet, Alert, TouchableHighlight } from 'react-native';
import Swipeout from 'react-native-swipeout';
import flatListData from '../data/flatListData';
import AddModal from './AddModal';
import EditModal from './EditModal';
class FlatListItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeRowKey: null,
      numberOfRefresh: 0,
    };
  }
  refreshFlatListItem = () => {
    this.setState((prevState) => {
      return {
        numberOfRefresh: prevState.numberOfRefresh + 1,
      };
    });
  };
  render() {
    const swipeSetting = {
      autoClose: true,
      onClose: (secId, rowId, direction) => {
        if (this.state.activeRowKey != null) {
          this.setState({ activeRowKey: null });
        }
      },
      onOpen: (secId, rowId, direction) => {
        this.setState({ activeRowKey: this.props.item.key });
      },
      right: [
        {
          onPress: () => {
            this.props.parentFlatList.refs.editModal.showEditModal(flatListData[this.props.index], this);
          },
          text: 'Edit', type: 'primary',
        },
        {
          onPress: () => {
            const deletingRow = this.state.activeRowKey;
            Alert.alert(
              'Alert',
              'Are you sure you want to Delete?',
              [
                {
                  text: 'Yes', onPress: () => {
                    flatListData.splice(this.props.index, 1);
                    //refresh FlatList
                    this.props.parentFlatList.refreshFlatList(deletingRow);
                  }
                },
                {
                  text: 'No', onPress: () => {

                  }
                }
              ],
              { cancelable: true }
            );
          },
          text: 'Delete', type: 'delete'
        }
      ],
      rowId: this.props.index,
      sectionId: 1,
    };

    return (
      <Swipeout {...swipeSetting}>
        <View style={{
          flex: 1,
          flexDirection: 'row',
          backgroundColor: 'green',
          borderBottomWidth: 1,
          borderColor: 'white'
        }}>
          <Image
            source={{ uri: this.props.item.imageUrl }}
            style={{ width: 100, height: 100, margin: 5 }} />
          <View style={{ flexDirection: 'column' }}>
            <Text style={styles.flatListItem}>{this.props.item.name}</Text>
            <Text style={styles.flatListItem}>{this.props.item.championDescription}</Text>
          </View>
        </View>
      </Swipeout>
    )
  }
}
const styles = StyleSheet.create({
  flatListItem: {
    color: 'white',
    padding: 10,
    fontSize: 16,
  }
})
class TextName extends Component {
  render() {
    const stringName = `Muốn ${this.props.textName}? `;
    return (
      <Text
        style={{ color: 'white' }}>{stringName}</Text>
    );
  }
}

export default class BasicFlatList extends Component {
  constructor(props) {
    super(props);
    this.state = ({
      deletedKey: null,
    });
    //lệnh Bind để biến this ở trong hàm _onPressAdd thành this trong BasicFlatList  
    this._onPressAdd = this._onPressAdd.bind(this);
  }
  refreshFlatList = (activeKey) => {
    this.setState((prevState) => {
      return {
        deletedRowKey: activeKey,
      };
    });
    this.refs.flatList.scrollToEnd();
  }
  _onPressAdd() {
    this.refs.addModal.showAddModal();
    // this.refs.addModal.showAddModal(); - this ở đây đang trỏ đến Button chứa hàm này
  }
  render() { 
    return (
      <View>
        <View style={{
          backgroundColor: 'tomato',
          height: 64,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'flex-end'
        }}>
          <TextName textName="Thêm champion " />
          <TouchableHighlight
            style={{ marginRight: 10 }}
            underlayColor='tomato'
            onPress={this._onPressAdd}
          >
            <Image
              style={{ width: 35, height: 35 }}
              source={{ uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSQql7r9OXEafcnXQI7vYnZgb6cBvq-QQQ1rXvXmRrE_kBN3yB8&usqp=CAU' }} />
          </TouchableHighlight>
        </View>
        <FlatList
          ref={"flatList"}
          data={flatListData}
          renderItem={({ item, index }) => {
            return (
              <FlatListItem item={item} index={index} parentFlatList={this}>

              </FlatListItem>
            )
          }}
        >

        </FlatList>
        <AddModal ref={'addModal'} parentFlatList={this} />
        <EditModal ref={'editModal'} parentFlatList={this} />
      </View>
    );
  }
}
