import React, { Component } from 'react';
import { View, FlatList, Text, StyleSheet, Image } from 'react-native';
import flatListData from '../data/flatListData';

class HorizontalFlatListItem extends Component {
  render() {
    return (
      <View style={{ flex: 1, width: 130, height: 130 }}>
        <Image style={styles.image}
          source={{ uri: "https://storage.oxii.vn/Cache/Sites/OXII/Storage/Images/2019/11/20/1920/lo-dien-tuong-moi-aphelios-trong-lmht-1.jpg" }} />
      </View>
    );
  }
}
export default class HorizontalFlatList extends Component {
  render() {
    return (
      <View 
      style={{
        flex: 1,
        flexDirection: 'column',
        marginTop: 20,
        backgroundColor: 'red',
        height: 130,
      }}>
        <FlatList
          horizontal={true}
          data={flatListData}
          renderItem={({ item, index }) => {
            return (
              <HorizontalFlatListItem item={item} index={index}
                parentFlatList={this} >

              </HorizontalFlatListItem>
            )
          }} >

        </FlatList>

      </View>
    )
  }
}

const styles = StyleSheet.create({
  image: {
    flex: 1,
    width: null,
    height: null,
    // borderRadius: 50,
    margin: 5,
    resizeMode: 'cover',
  }
})