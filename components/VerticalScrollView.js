import React, { Component } from 'react';
import {
  ScrollView, Dimensions, Button, Text, Image, TextInput,
  ViewPagerAndroid
} from 'react-native';

export default class VerticalScrollView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      textSomething: ''
    }
  }
  render() {
    const urlImage = "https://lienminh.garena.vn/images/Lan_h3lpm3/08_2018/Thresh_HighNoonSkin.jpg";
    let screenWidth = Dimensions.get('window').width;
    return (
      <ScrollView
        keyboardDismissMode='on-drag'
        // horizontal={true}
        maximumZoomScale={2}
        showsVerticalScrollIndicator={true}
      >
        <Image
          source={{ uri: urlImage }}
          style={{
            width: screenWidth,
            height: screenWidth * 717 / 1215,
            marginTop: 10
          }}
        />
        <Text
          style={{
            textAlign: 'center',
            padding: 10,
            fontSize: 16,
            backgroundColor: 'dodgerblue',
            color: 'white'
          }}
        >Thresh High Noon</Text>

        <TextInput
          placeholder='Write something ...'
          style={{
            padding: 5,
            margin: 5,
            borderWidth: 1
          }}
          onChangeText={(text) => { this.setState({ textSomething: text }) }}
        />
        <Button
          title='alert'
          onPress={() => { alert(this.state.textSomething) }}
        />
        <Image
          source={{ uri: urlImage }}
          style={{
            width: screenWidth,
            height: screenWidth * 717 / 1215,
            marginTop: 10
          }}
        />
        <Image
          source={{ uri: urlImage }}
          style={{
            width: screenWidth,
            height: screenWidth * 717 / 1215,
            marginTop: 10
          }}
        />
        <Image
          source={{ uri: urlImage }}
          style={{
            width: screenWidth,
            height: screenWidth * 717 / 1215,
            marginTop: 10
          }}
        />
        <Image
          source={{ uri: urlImage }}
          style={{
            width: screenWidth,
            height: screenWidth * 717 / 1215,
            marginTop: 10
          }}
        />
        <Image
          source={{ uri: urlImage }}
          style={{
            width: screenWidth,
            height: screenWidth * 717 / 1215,
            marginTop: 10
          }}
        />
      </ScrollView>
    )
  }
}